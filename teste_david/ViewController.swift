//
//  ViewController.swift
//  teste_david
//
//  Created by COTEMIG on 28/04/22.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:    )
        
        if indexPath.row % 2 == 0{
            cell.backgroundColor = .red
        }
        
        return cell
    }
    
    @IBOutlet weak var tabela: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tabela.dataSource = self
    
    }


}

